# code@mms Recommended Software and Services List
Welcome to the code@mms software list! This list is maintained by the club leader and assistant. Below, you will find a list of programs that we recommend to use for the best functionality and so you can receive better assistance.

## List

### Software
* [Android Studio](https://developer.android.com/studio/index.html)
  * We are currently testing if this can be replaced with IntelliJ IDEA 
* [Atom](https://atom.io/)
  * Add-ons will be covered later
* [Discord](https://discordapp.com)
* [Figma UI](https://figma.com)
* [Firefox Beta](https://www.mozilla.org/en-US/firefox/channel/desktop/) or [Firefox Developer](https://www.mozilla.org/en-US/firefox/channel/desktop/)
  * Add-ons will be covered later
  * Note: Chrome is okay, but Firefox is preferable due to open source and non Google integration.
* [Git](https://git-scm.com/)
  * For Linux machines, get the latest Git from your package manager.
* [Inkscape](https://inkscape.org)
* [IntelliJ IDEA](https://www.jetbrains.com/idea/)
* [Node.js](https://nodejs.org)

### Services
* [GitLab](https://gitlab.com)
* [GitHub](https://github.com)
* [MEGA](https://mega.nz)
* Email
  * Please message Daniel (daniel#5289) on Discord with your email and name so we can add you to the mailing list.

### Add-ons for Firefox (XPI)
* **NOTE: These are searchable in the Add-ons section.**
* [HTTPS Everywhere](https://www.eff.org/files/https-everywhere-test/https-everywhere-latest.xpi)
  * The version of HTTPS Everywhere in the Add-ons section is outdated. Get the beta above.
* [Privacy Badger](https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17)
* [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin)
* [Markdown Viewer](https://addons.mozilla.org/en-US/firefox/addon/markdown-viewer-webext)
  * Please find the WebExtension version of this, as other versions will not work. WebExt Link above.
* [MEGA](https://mega.nz/meganz.xpi)

### Add-ons for Atom
* **NOTE: These are searchable in the Add-ons section.**
* [Atom IDE UI](https://atom.io/packages/atom-ide-ui)
  * Also make sure to get any add-on packages for languages you're using.
* [Solarized Dark Syntax](https://atom.io/themes/solarized-dark-syntax)
  * This should be preinstalled with Atom itself.
* [One Dark Theme](https://atom.io/themes/one-dark-ui)
  * This should be preinstalled with Atom itself.
* Molecule (WIP) (In-house add-on for collaboration)
  * We are working on the add-on, but we need you guys to do it for us. Isn't released as of now.

### Other
* **[Optional]** quickCommit scripts
  * You can find them inside of the **software** folder in this repository, or the **root** folder of this repo, which are used to commit.
  * Just execute one that fits your OS and done.
* **[Optional]** San Francisco Fonts (Ask Daniel for files.)
* Common sense
* Good work ethic

Thank you for reading! If you feel like you can add to this list, create a pull request with your additions or create an issue and we'll get to you as soon as possible.
